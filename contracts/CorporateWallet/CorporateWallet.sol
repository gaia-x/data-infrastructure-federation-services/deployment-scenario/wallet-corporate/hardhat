// SPDX-License-Identifier: None
pragma solidity ^0.8.0;

import "./CredentialStorage.sol";

contract CorporateWallet is CredentialStorage {
    struct CredentialReturn {
        string credentialId;
        string url;
    }

    mapping(string => string[]) private companyToCredentialIds; // Mapping from company ID to list of credential IDs.

    /**
     * @param credentialId string: ID of the credential added.
     * @param url string: URL of the credential.
     * @param company string: Company to which the credential was added.
     */
    event CredentialAdded(string credentialId, string url, string company);
    /**
     * @param credentialId string: ID of the credential removed.
     * @param company string: Company from which the credential was removed.
     */
    event CredentialRemoved(string credentialId, string company);

    constructor() {}

    /**
     * @dev Adds a new credential to the company of the caller.
     * @param credentialId string memory: The unique ID for the new credential.
     * @param uri string memory: The URL where the credential can be accessed.
     */
    function addCredentialToCompany(string memory credentialId, string memory uri) public onlyCompany onlyAdministrator {
        _addCredentialToCompany(credentialId, uri);
    }

    /**
     * @dev Adds multiple credentials to the caller's company.
     * @param credentialIds string[] memory: Array of credential IDs.
     * @param uris string[] memory: Array of URLs where the credentials can be accessed.
     */
    function addMultipleCredentialsToCompany(string[] memory credentialIds, string[] memory uris) public onlyCompany {
        require(credentialIds.length == uris.length, "The arrays must have the same length.");
        for (uint i = 0; i < uris.length; i++) {
            _addCredentialToCompany(credentialIds[i], uris[i]);
        }
    }

    function _addCredentialToCompany(string memory credentialId, string memory uri) internal {
        _addCredential(credentialId, uri);

        string memory company = employeeToCompany[msg.sender];
        companyToCredentialIds[company].push(credentialId);
        emit CredentialAdded(credentialId, uri, company);
    }

    /**
     * @dev Removes a credential from the caller's company.
     * @param credentialId memory string: The ID of the credential to be removed.
     */
    function removeCredentialFromCompany(string memory credentialId) public onlyCompany onlyAdministrator {
        string memory company = employeeToCompany[msg.sender];
        require(companyToCredentialIds[company].length > 0, "No credential for this company.");

        uint256 index = _findCredentialIndex(credentialId, company);
        require(index < companyToCredentialIds[company].length, "Credential not found.");

        for (uint i = index; i < companyToCredentialIds[company].length - 1; i++) {
            companyToCredentialIds[company][i] = companyToCredentialIds[company][i + 1];
        }
        companyToCredentialIds[company].pop();
        delete credentials[credentialId];
        emit CredentialRemoved(credentialId, company);
    }

    function _findCredentialIndex(string memory credentialId, string memory company) private view returns (uint256) {
        for (uint256 i = 0; i < companyToCredentialIds[company].length; i++) {
            if (keccak256(bytes(companyToCredentialIds[company][i])) == keccak256(bytes(credentialId))) {
                return i;
            }
        }

        return companyToCredentialIds[company].length;
    }

    /**
     * @dev Retrieves all credentials associated with the caller's company.
     * @return CredentialReturn[] memory: Array of CredentialReturn containing details of each credential.
     */
    function getMyCompanyCredentials() public view onlyCompany returns (CredentialReturn[] memory) {
        return getCompanyCredentials(employeeToCompany[msg.sender]);
    }

    /**
     * @dev Retrieves all credentials associated with the company of a specified employee address.
     * @param employeeAddress address: Address of the employee whose company credentials are to be retrieved.
     * @return CredentialReturn[] memory: Array of CredentialReturn containing details of each credential.
     */
    function getCompanyCredentialsFromAddress(address employeeAddress) public view onlyCompany returns (CredentialReturn[] memory) {
        return getCompanyCredentials(employeeToCompany[employeeAddress]);
    }

    /**
     * @dev Retrieves all credentials associated with a specific company.
     * @param company string memory: The company ID whose credentials are to be retrieved.
     * @return CredentialReturn[] memory: Array of CredentialReturn containing details of each credential.
     */
    function getCompanyCredentials(string memory company) public view returns (CredentialReturn[] memory) {
        string[] memory credentialIds = companyToCredentialIds[company];
        CredentialReturn[] memory tempCredentials = new CredentialReturn[](credentialIds.length);
        uint256 count = 0;

        for (uint256 i = 0; i < credentialIds.length; i++) {
            if (checkAccess(credentialIds[i])) {
                Credential storage credential = credentials[credentialIds[i]];
                tempCredentials[count] = CredentialReturn({
                    credentialId: credential.credentialId,
                    url: credential.url
                });
                count++;
            }
        }

        CredentialReturn[] memory authorizedCredentials = new CredentialReturn[](count);
        for (uint256 j = 0; j < count; j++) {
            authorizedCredentials[j] = tempCredentials[j];
        }

        return authorizedCredentials;
    }

}
