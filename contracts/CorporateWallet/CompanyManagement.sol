// SPDX-License-Identifier: None
pragma solidity ^0.8.0;

import "./PermissionManager.sol";

contract CompanyManagement is PermissionManager {
    struct Company {
        string id;
        string name;
    }

    struct Employee {
        string email;
        bool isAdmin;
    }
    string[] private registeredCompanies; //Array of all registered company IDs.
    mapping(string => bool) public isCompanyRegistred; //Mapping to check if a company is registered.
    mapping(string => Company) public companies; //Mapping of company IDs to Company struct.
    mapping(address => string) public employeeToCompany; //Mapping from employee address to their company ID.
    mapping(string => address[]) public companyEmployees; //Mapping from company ID to list of employee addresses.
    mapping(address => Employee) public employees; //Mapping from employee address to Employee struct.

    /**
     * @dev Restricts function access to registered company employees.
     */
    modifier onlyCompany() {
        require(isCompanyRegistred[employeeToCompany[msg.sender]], "Caller is not a registered company");
        _;
    }

    /**
     * @dev Restricts function access to employees with Admin status.
     */
    modifier onlyAdministrator() {
        require(employees[msg.sender].isAdmin, "Not authorized.");
        _;
    }

    /**
     * @param employeeAddress address: Address of the newly registered employee.
     */
    event EmployeeRegistered(address employeeAddress);

    /**
     * @dev Registers a new company and the sender as its functional administrator.
     * @param companyId string memory: The unique identifier for the new company.
     * @param companyName string memory: The name of the new company.
     */
    function registerCompany(string memory companyId, string memory companyName) public {
        require(!isCompanyRegistred[companyId], "Company already registered");
        companies[companyId] = Company(companyId, companyName);
        registeredCompanies.push(companyId);
        isCompanyRegistred[companyId] = true;

        joinCompany(companyId, "");

        Permission memory fullPermission = Permission({
            read: true,
            consume: true,
            present: true,
            importC: true,
            walletCorpo: true
        });
        setUserPermission(msg.sender, fullPermission);
        employees[msg.sender].isAdmin = true;
    }

    /**
     * @dev Retrieves lists of all registered companies and their names.
     * @return string[] memory: Array of registered company IDs.
     * @return string[] memory: Array of registered company names.
     */
    function getRegisteredCompanies() public view returns (string[] memory, string[] memory) {
        string[] memory companyNames = new string[](registeredCompanies.length);
        for (uint i = 0; i < registeredCompanies.length; i++) {
            companyNames[i] = companies[registeredCompanies[i]].name;
        }
        return (registeredCompanies, companyNames);
    }

    /**
     * @dev Allows an existing employee to join a registered company.
     * @param companyId string memory: ID of the company to join.
     * @param employeeEmail string memory: Email of the employee joining.
     */
    function joinCompany(string memory companyId, string memory employeeEmail) public {
        require(isCompanyRegistred[companyId], "Company is not registered");
        require(keccak256(bytes(employeeToCompany[msg.sender])) != keccak256(bytes(companyId)), "Employee already in this company");

        employeeToCompany[msg.sender] = companyId;
        companyEmployees[companyId].push(msg.sender);

        Employee memory newEmployee = Employee({
            email: employeeEmail,
            isAdmin: false
        });
        employees[msg.sender] = newEmployee;

        emit EmployeeRegistered(msg.sender);
    }

    /**
     * @dev Adds a new employee to the sender's company.
     * @param employeeAddress address: Ethereum address of the new employee.
     * @param employeeEmail memory string: Email of the new employee.
     */
    function addEmployeeToCompany(address employeeAddress, string memory employeeEmail) public onlyCompany {
        string memory companyId = employeeToCompany[msg.sender];
        require(keccak256(bytes(employeeToCompany[employeeAddress])) != keccak256(bytes(companyId)), "Employee already in this company");

        employeeToCompany[employeeAddress] = companyId;
        companyEmployees[companyId].push(employeeAddress);

        Employee memory newEmployee = Employee({
            email: employeeEmail,
            isAdmin: false
        });
        employees[employeeAddress] = newEmployee;

        emit EmployeeRegistered(employeeAddress);
    }

    /**
     * @dev Updates the email address of an existing employee.
     * @param employeeAddress address: Ethereum address of the employee whose email is to be updated.
     * @param email string memory: New email address.
     */
    function updateEmployeeEmail(address employeeAddress, string memory email) public onlyCompany {
        employees[employeeAddress].email = email;
    }

    /**
     * @dev Retrieves all employees of the caller's company.
     * @return address[] memory: Array of employee addresses.
     * @return Employee[] memory: Array of Employee struct containing employee details.
     */
    function getEmployeesOfMyCompany() public view onlyCompany returns (address[] memory, Employee[] memory, Permission[] memory) {
        string memory companyId = employeeToCompany[msg.sender];
        address[] memory employeeAddresses = companyEmployees[companyId];
        Employee[] memory employeeData = new Employee[](employeeAddresses.length);
        Permission[] memory employeePermissions = new Permission[](employeeAddresses.length);

        for (uint i = 0; i < employeeAddresses.length; i++) {
            employeeData[i] = employees[employeeAddresses[i]];
            employeePermissions[i] = userPermissions[employeeAddresses[i]];
        }

        return (employeeAddresses, employeeData, employeePermissions);
    }

    /**
    * @dev Retrieve employee's email.
    * @param employeeAddress address: Ethereum address of the employee.
    * @return string: Email of an employee.
    */
    function employeeAddressToEmail(address employeeAddress) public view returns (string memory) {
        return employees[employeeAddress].email;
    }

    /**
    * @dev Set an employee admin
    * @param employeeAddress address: Ethereum address of the employee.
    */
    function setIsAdmin(address employeeAddress, bool isAdmin) public onlyAdministrator {
        employees[employeeAddress].isAdmin = isAdmin;
    }
}
