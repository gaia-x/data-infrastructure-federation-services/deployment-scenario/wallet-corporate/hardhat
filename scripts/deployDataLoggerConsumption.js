const hre = require("hardhat");

async function main() {
    const logger = await hre.ethers.deployContract("DataLoggerConsumption", []);

    await logger.waitForDeployment();

    console.log(`DataLoggerConsumption deployed to: ${logger.target}`);
}

main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});