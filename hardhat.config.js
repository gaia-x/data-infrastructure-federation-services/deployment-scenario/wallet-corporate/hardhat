require("@nomicfoundation/hardhat-toolbox");
require("@nomicfoundation/hardhat-ignition-ethers");

module.exports = {
    defaultNetwork: "hardhat",
    networks: {
        hardhat: {
            gas: 0,
            gasPrice: 0,
            initialBaseFeePerGas: 0,
            allowUnlimitedContractSize: true,
        }
    },
    solidity: {
        version: "0.8.24",
        settings: {
            optimizer: {
                enabled: true,
                runs: 200
            }
        }
    },
    paths: {
        sources: "./contracts",
        tests: "./test"
    }
};