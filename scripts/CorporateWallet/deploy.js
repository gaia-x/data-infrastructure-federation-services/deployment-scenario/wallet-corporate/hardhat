const hre = require("hardhat");

async function main() {
  const corporateWallet = await hre.ethers.deployContract("CorporateWallet", {});

  await corporateWallet.waitForDeployment();

  console.log(`CorporateWallet deployed to: ${corporateWallet.target}`);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
