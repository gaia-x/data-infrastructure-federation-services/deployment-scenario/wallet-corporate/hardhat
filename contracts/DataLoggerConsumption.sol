// SPDX-License-Identifier: MIT
pragma solidity ^0.8.24;

contract DataLoggerConsumption {
    // Structure pour stocker les informations
    struct Data {
        string userId;
        string date;
        string ipAddress;
        string fileName;
        string fileSize;
        string fileType;
    }

    // Mapping pour stocker les données; clé est le userId, valeur est un tableau de Data
    mapping(string => Data[]) private dataStore;

    // Fonction pour écrire des données sur la blockchain
    function writeData(
        string memory _userId,
        string memory _date,
        string memory _ipAddress,
        string memory _fileName,
        string memory _fileSize,
        string memory _fileType
    ) public {
        // Création de l'objet Data
        Data memory newData = Data({
            userId: _userId,
            date: _date,
            ipAddress: _ipAddress,
            fileName: _fileName,
            fileSize: _fileSize,
            fileType: _fileType
        });

        // Ajouter les données au stockage
        dataStore[_userId].push(newData);
    }

    // Fonction pour récupérer les données en fonction du userId et d'un intervalle de dates
    function retrieveData(
        string memory _userId,
        string memory _startDate,
        string memory _endDate
    ) public view returns (Data[] memory) {
        Data[] memory tempData = new Data[](dataStore[_userId].length);
        uint count = 0;

        // Filtrer les données en fonction de la plage de dates
        for (uint i = 0; i < dataStore[_userId].length; i++) {
            if (compareDates(dataStore[_userId][i].date, _startDate) >= 0 &&
                compareDates(dataStore[_userId][i].date, _endDate) <= 0) {
                tempData[count] = dataStore[_userId][i];
                count++;
            }
        }

        // Créer un tableau pour les résultats avec la taille exacte
        Data[] memory results = new Data[](count);
        for (uint i = 0; i < count; i++) {
            results[i] = tempData[i];
        }

        return results;
    }

    // Fonction d'assistance pour comparer les dates (format YYYY-MM-DD)
    function compareDates(string memory _date1, string memory _date2) private pure returns (int) {
        // Conversion des dates en entiers pour comparaison
        return parseInt(_date1) - parseInt(_date2);
    }

    // Fonction d'assistance pour convertir string en int
    function parseInt(string memory _value) private pure returns (int) {
        bytes memory temp = bytes(_value);
        int result = 0;
        for (uint i = 0; i < temp.length; i++) {
            // Ignorer les tirets
            if (temp[i] != "-") {
                uint digit = uint(uint8(temp[i]) - 48); // Convertir le caractère ASCII en chiffre
                require(digit <= 9, "Invalid input; must be digits or dash only");
                result = result * 10 + int(digit);
            }
        }
        return result;
    }
}
