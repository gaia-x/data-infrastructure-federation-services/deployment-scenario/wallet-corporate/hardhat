
# Configuration

**To be done in a WSL2 environment**

Hardhat from the Gaia repository:
```shell
git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/wallet-corporate/hardhat.git
cd hardhat
```

Useful link: https://hardhat.org/hardhat-runner/docs/getting-started#overview

## Compilation
```shell
npx hardhat compile
```
This automatically compiles all the .sol files in the hardhat/contracts folder.

## Deployment
```shell
npx hardhat run scripts/deploy.js
```
Be careful with the contents of deploy.js. It must contain the information of the contract to be deployed.

When the network is launched locally, the procedure is different; you have to initiate the deployment by specifying the network:
```shell
npx hardhat run scripts/deploy.js --network localhost
```

# Creation of a Testnet

Now, to run a testnet locally, we will initialize the first Hardhat node by executing the following command:
```shell
npx hardhat node
```

This will then allow us to connect to our blockchain network on MetaMask, for example.
http://127.0.0.1:8545

Adding a network on MetaMask:
Add a network > Add a network manually > fill in the information

![selectRes.png](ressources/img/selectRes.png)
![addRes.png](ressources/img/addRes.png)
![infoRes.png](ressources/img/infoRes.png)
